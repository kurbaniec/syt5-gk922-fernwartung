## Kacper Urbaniec | 5AHIT | 08.10.2019

# "*Fernwartung*"

#### 1. Einführung  

Secure Shell oder SSH ermöglicht eine sichere, authentifizierte und verschlüsselte Verbindung zwischen zwei Rechnern über ein unsicheres Netzwerk.  

#### 1.2 Ziele  

1. OpenSSH Public Key Authentifizierung unter Ubuntu   
2. Mit VNC und SSH eine sichere Verbindung aufbauen   

#### 1.2 Voraussetzungen   	

#### Grundlagen Linux    Virtualisierungsumgebung Ubuntu    

#### 1.3	Aufgabenstellung 

1. Es soll ein SSH-Zugang für eine Authentifizierung mittels Public-Key-Verfahren konfiguriert werden. Dazu soll am Client ein Schlüsselpaar erstellt, der öffentliche Teil der Schlüssel auf den Server übertragen und anschließend der Server für die Schlüssel-Authentifizierung eingerichtet werden. Anschließend soll der Benutzer ohne Login-Passwort am Server anmelden können. (GK, 0,25 EH) https://www.thomas-krenn.com/de/wiki/OpenSSH_Public_Key_Authentifizierung_unter_Ubuntu 
2. Es soll ein sicherer Fernzugriff via TightVNC zu einem Linux-Server ermöglicht werden. Dazu soll SSH-Tunnel eingesetzt werden. (GK, 0,25 EH) https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-vnc-on-ubuntu-18-04

- **Windows-Client mit einem Ubuntu Server-per VNC und SSH:**  https://www.theurbanpenguin.com/creating-an-ssh-tunnel-with-putty-to-secure-vnc/     
- **Linux mit einem Ubuntu Server-per VNC und SSH:**   ssh -L 5901:127.0.0.1:5901 -N -f -l username server_ip_address 

#### 2. Einführung 

Internet Protocol Security (IPsec) ist eine Protokoll-Suite, die eine gesicherte Kommunikation über potentiell unsichere IP-Netze wie das Internet ermöglichen soll. [Wiki]

#### **2.1 Ziele** 

Abschirmung der Datenverbindung 

####  2.2 Voraussetzungen

#### Grundlagen Linux  Virtualisierungsumgebung Ubuntu  

#### 2.3 Aufgabenstellung 

Es soll eine abgeschirmte Datenverbindung über ein IPsec Site-to-Site VPN (PSK) eingerichtet werden. Anschließend sollen der erfolgreiche Aufbau und die Verschlüsselung der Verbindung mit einem Ping getestet werden. Dazu soll Server_A vom Server_B angepingt werden und am Server_A soll tcpdump mit **sudo tcpdump esp** gestartet werden. Als Ergebnis sollte am Server_A die Verschlüsselte ESP Verbindung zu sehen sein. 

(GK, 0,5 EH) https://console.kim.sg/strongswan-ipsec-vpn-with-pre-shared-key-and-certificates/

## Implementierung

### Aufgabe 1

Zuerst wird am Client der Key für SSH generiert:

```
ssh-keygen -b 4096
# (Optional) Filenamen angeben /home/kurbaniec/.ssh/key_rsa
# (Optional) Passphrase eingeben 12345
```

Dieser Key muss dann am Server autorisiert werden:

```
ssh-copy-id -i .ssh/key_rsa.pub kurbaniec@192.168.84.129
```

`kurbaniec@192.168.84.129` bezeichnet in diesem Fall meine VM (= Server A).

Beim Vorgang muss das Passwort des Users `kurbaniec` des Servers eingegeben werden, damit sich das System anmeldet und den Key zum Server hinzufügt. 

Anmelden sollte jetzt wie folgt möglich sein:

```
ssh -i .ssh/key_rsa kurbaniec@192.168.84.129
```

Dabei wird, je nachdem ob man eine Passphrase für das Keyfile erstellt, nach dieser gefragt.

### Aufgabe 2

Als VNC-Client habe ich auf meiner zweiten virtuellen Maschine `vinagre` installiert:

```
sudo apt-get install vinagre
```

Server-Konfig:

```
sudo apt update
sudo apt install xfce4 xfce4-goodies
sudo apt install tightvncserver
```

VNC-Server konfigurieren

```
vncserver
```

Es wird nach einem Password gefragt, dieses muss zwischen 6 und 8 Zeichen sein, z.B. `123456`.

Nach erfolgreicher Konfiguration sollte folgende Ausgabe erscheinen:

```
New 'X' desktop is ubuntu:1

Creating default startup script /home/kurbaniec/.vnc/xstartup
Starting applications specified in /home/kurbaniec/.vnc/xstartup
Log file is /home/kurbaniec/.vnc/ubuntu:1.log
```

Als nächstes stoppen wir den VNC-Server um weitere Konfigurationen zu machen.

```
# Stop server
vncserver -kill :1
# Backup files
mv ~/.vnc/xstartup ~/.vnc/xstartup.bak
```

Jetzt wird ein Startup-Skript erstellt, dass bei jedem Start von VNC-Server ausgeführt und die GUI starten soll:

```
nano ~/.vnc/xstartup
```

Inhalt `~/.vnc/xstartup`:

```
#!/bin/bash
xrdb $HOME/.Xresources
startxfce4 &
```

Jetzt muss das Skript ausführbar gemacht werden und der VNC-Server kann wieder gestartet werden:

```
sudo chmod +x ~/.vnc/xstartup
vncserver
```

Jetzt kann VNC-Server am Client ausprobiert werden.

Dazu startet man auf diesen einen SSH-Tunnel und bindest den lokalen Port 5901 zum Server Port 5901 wo die VNC Verbindung laufen wird.

```
ssh -L 5901:127.0.0.1:5901 -C -N -l kurbaniec 192.168.84.129
```

Jetzt starte am Client die Applikation mit dem Befehl `vigrante` und starte die Sitzung, in dem ich die Adresse `localhost:5901` eintrage und dann das VNC-Password angebe: 123456:

![](img/vnc_1.PNG)

![](img/vnc_2.PNG)

Jetzt sollte ein Desktop angezeigt werden:

![](img/vnc_4.PNG)

Falls kein Desktop, sondern nur ein graues Rechtecke angezeigt wird, dann ist das kein Problem. Einfach weiter die Anleitung hier befolgen, wenn man VNC dann als Service startet sollte die GUI funktionieren.

![](img/vnc_3.PNG)

Als nächstes konfigurieren VNC als ein System-Service.

```
sudo nano /etc/systemd/system/vncserver@.service
```

Inhalt von `/etc/systemd/system/vncserver@.service `:

```
[Unit]
Description=Start TightVNC server at startup
After=syslog.target network.target

[Service]
Type=forking
User=kurbaniec
Group=kurbaniec
WorkingDirectory=/home/kurbaniec

PIDFile=/home/kurbaniec/.vnc/%H:%i.pid
ExecStartPre=-/usr/bin/vncserver -kill :%i > /dev/null 2>&1
ExecStart=/usr/bin/vncserver -depth 24 -geometry 1280x800 :%i
ExecStop=/usr/bin/vncserver -kill :%i

[Install]
WantedBy=multi-user.target
```

Jetzt fügen wird die Konfiguration dem System hinzu:

```
sudo systemctl daemon-reload
sudo systemctl enable vncserver@1.service
```

Jetzt probieren wir die Konfiguration aus.

Zuerst wird der VNC-Server ausgeschalten und dann per `systemctl` wieder gestartet.

```
vncserver -kill :1
sudo systemctl start vncserver@1
sudo systemctl status vncserver@1
```

Jetzt sollte alles funktionieren.

### Aufgabe 3

Zuerst musst Strongswan auf beiden Servern installiert werden:

```
sudo apt-get install strongswan
sudo apt-get install haveged
sudo systemctl enable haveged
sudo systemctl start haveged
```

Jetzt wird das Routing aktiviert (auf beiden Servern):

```
sudo nano /etc/sysctl.conf
```

Inhalt von `sudo nano /etc/sysctl.conf`:

```
net.ipv4.ip_forward = 1 
net.ipv4.conf.all.accept_redirects = 0 
net.ipv4.conf.all.send_redirects = 0
```

### Pre-shared key config

Um die VPN-Verbindung zu erstellen müssen folgende Dateien konfiguriert werden.

Server A:

```
sudo nano /etc/ipsec.secrets
```

Inhalt von `/etc/ipsec.secrets`:

```
192.168.84.129 192.168.84.128 : PSK 'password123'
```

Server B:

```
sudo nano /etc/ipsec.secrets
```

Inhalt von `/etc/ipsec.secrets`:

```
192.168.84.128 192.168.84.129 : PSK 'password123'
```

Server A:

```
sudo nano /etc/ipsec.conf
```

Inhalt von `/etc/ipsec.conf`:

```
conn A_TO_B
 authby=secret
 left=192.168.84.129
 leftsubnet=192.168.84.0/24
 right=192.168.84.128
 rightsubnet=192.168.84.0/24
 ike=aes256-sha2_256-modp1024!
 esp=aes256-sha2_256!
 keyingtries=0
 ikelifetime=1h
 lifetime=8h
 dpddelay=30
 dpdtimeout=120
 dpdaction=restart
 auto=start
```

Server B:

```
sudo nano /etc/ipsec.conf
```

Inhalt von `/etc/ipsec.conf`:

```
conn B_TO_A
 authby=secret
 left=192.168.84.129
 leftsubnet=192.168.84.0/24
 right=192.168.84.128
 rightsubnet=192.168.84.0/24
 ike=aes256-sha2_256-modp1024!
 esp=aes256-sha2_256!
 keyingtries=0
 ikelifetime=1h
 lifetime=8h
 dpddelay=30
 dpdtimeout=120
 dpdaction=restart
 auto=start
```

Jetzt müssen die iptables konfiguriert werden, so dass Pakete zwischen Server A und B nicht mit NAT behandelt werden.

Server A:

```
sudo iptables -t nat -I POSTROUTING ! -d 192.168.84.0/24 -o wan -j MASQUERADE
sudo iptables -t nat -I POSTROUTING -s 192.168.84.0/24 ! -d 192.168.84.0/24 -o wan -j MASQUERADE
sudo iptables-restore < /etc/iptables/rules.v4
```

Server B:

```
sudo iptables -t nat -I POSTROUTING ! -d 192.168.84.0/24 -o wan -j MASQUERADE
sudo iptables -t nat -I POSTROUTING -s 192.168.84.0/24 ! -d 192.168.84.0/24 -o wan -j MASQUERADE
sudo iptables-restore < /etc/iptables/rules.v4
```

Jetzt startet man ipsec auf beiden Servern neu und liest die Konfiguration nochmals ein:

```
ipsec restart
ipsec rereadsecrets
```

Um die Konfiguration zu testen startet man die Verbindung zwischen den beiden Servern:

Server A:

```
sudo ipsec up B_TO_A
```

Server B:

```
sudo ipsec up A_TO_B
```

Auf Server B startet man jetzt einen Ping und führt am Server A `sudo tcpdump esp` aus. Man sollte circa folgenden Output erhalten:

![](img/ipsec.PNG)



## Quellen

* [SSH mit Key-Authentifizierung | 08.10.2019](https://www.thomas-krenn.com/de/wiki/OpenSSH_Public_Key_Authentifizierung_unter_Ubuntu)
* [StrongSwan ipsec VPN | 08.10.2109](https://console.kim.sg/strongswan-ipsec-vpn-with-pre-shared-key-and-certificates/)